#FastAPI
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from fastapi import Cookie
#Python
from datetime import datetime, timedelta
from passlib.context import CryptContext
#Pydantic
from pydantic import BaseModel
#Services
from jose import JWTError, jwt
from users_service.app.models.users_models import User, UserOut
from users_service.app.config.db import db
import requests

#Constanst
SECRET_KEY = "0cf3d2c4c7014ca7e93a3108f549eb0bdb11781d7ec02f699373c70958d05d9b"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login")


def get_current_user(access_token:str = Cookie(None)):
    if access_token is None:
        raise HTTPException(status_code=401, detail="invalid token or user not logged in")
    try:
        payload = jwt.decode(access_token, SECRET_KEY, algorithms=[ALGORITHM])
        username: str = payload.get("username")
        if username is None:
            raise HTTPException(status_code=400, detail="Not Found User")
    except JWTError:
        raise HTTPException(status_code=401, detail='User not logged in')
    user = db.users.find_one({"username": username})
    if user is None:
        raise HTTPException(status_code=400, detail="Not Found User")

    return user['_id']


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)

    return encoded_jwt




# def get_current_user(token: str = Depends(oauth2_scheme)):

#     credentials_exception = HTTPException(
#         status_code=status.HTTP_401_UNAUTHORIZED,
#         detail="Could not validate credentials",
#         headers={"WWW-Authenticate": "Bearer"},
#     )
#     try:
#         payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
#         username: str = payload.get("username")
#         if username is None:
#             raise credentials_exception
#     except JWTError:
#         raise credentials_exception
#     user = db.users.find_one({"username": username})
#     if user is None:
#         raise credentials_exception

#     return user['_id']


# async def get_current_active_user(current_user: UserOut = Depends(get_current_user)):
#     if current_user.disabled:
#         raise HTTPException(status_code=400, detail="Inactive user")
#     return current_user
