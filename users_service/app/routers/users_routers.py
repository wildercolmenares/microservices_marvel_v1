from fastapi import APIRouter, status, Query, Body, HTTPException, Response, Depends
#Python
import requests
from typing import Optional
from datetime import datetime, timedelta
#Utils
from users_service.app.models.users_models import UserOut, User
from users_service.app.models.login_models import Login
from users_service.app.schemas.users_schema import userSchema, userList
from users_service.app.config.db import db
from passlib.context import CryptContext
from users_service.app.config.common import create_access_token, get_current_user


router = APIRouter(
    tags=["Admin Auth"]
)

#Variables
ACCESS_TOKEN_EXPIRE_MINUTES = 30
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

@router.post(
    "/register",
    status_code=status.HTTP_201_CREATED,
    response_model=UserOut,
    summary="Create a new user"
)
def register(user: User = Body(...)):
    """
    Create new user

    **Description:**
        This path operations create a new user
        
    
    **Parameters:**
    - name (string, required): name of the new user
    - age (string, required): age of the new user
    - username (string, required) username of the new user
    - password (string, required) password of the new user
    - password_confirm (string, required) confirm password of the new user

    **Returns:**
    - id (string) 
    - name (string)
    - age (string)
    - username (string)
    """

    #Validation username
    verify_user = db.users.find_one({'username': user.username})
    if verify_user:
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT,
            detail='This user exists.'
        )

    #Validation password
    if user.password != user.password_confirm:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail='Passwords do not match'
        )
    
    #Create a new user
    user.password = pwd_context.hash(user.password)
    user.disabled = True
    add_user = db.users.insert_one(user.dict())
    schema = userSchema(db.users.find_one({'_id': add_user.inserted_id}))

    return {"id": str(add_user.inserted_id), "user": schema}


@router.post("/login")
def login(
    login_user: Login,
    response: Response,
):

    """
    Create new user

    **Description:**
        This path operations create a new user
        
    
    **Parameters:**
    - username (string, required) username of the new user
    - password (string, required) password of the new user
   

    **Returns:**
    - id (string) : identifier in the record
    - name (string): User name
    - age (string): age of the user
    - token (string): unique token per user
    """

    #Validate user
    verify_user = db.users.find_one({'username': login_user.username})
    if not verify_user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail='User no exists'
        )
    
    user = db.users.find_one({'username': login_user.username})

    #validate password
    verify_password = pwd_context.verify(login_user.password, verify_user['password'])
    if not verify_password:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail='Wrong password'
        )
    

    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"username": user['username']}, expires_delta=access_token_expires
    )

    #Add cookie    
    # response.set_cookie(access_token)
    response.set_cookie(key='access_token', value=access_token)

    schema = userSchema(db.users.find_one({'username': login_user.username}))

    return {
        "id":schema['id'],
        "name":schema['name'],
        "age":schema['age'],
        'access_token':access_token,
        'token_type': "Bearer"
    }


@router.get(
    "/logout",
    status_code=status.HTTP_200_OK
    )
def logout(
    response: Response,
):
    """
    Logout user

    **Description:**
        This path operations logout user
        
    
    **Parameters:**

   

    **Returns:**
    - message (string) : successfully logged
    """
    response.delete_cookie("access_token")

    return {"message": "Successfully logged out"}


@router.get(
    "/users",
    status_code=status.HTTP_200_OK,
    summary="Get all users"
)
def users_all():
    """
    Get all users

    **Description:**
        This path operations get all users in the database
        
    
    **Parameters:**

   

    **Returns:**
    - List (dic) : List all users
    """

    users = userList(db.users.find())

    return users

@router.get(
    "/users/me",
    status_code=status.HTTP_200_OK,
    summary="Get my data"
)
def read_users_me(
    user_id: str = Depends(get_current_user)
):
    """
    Get data user

    **Description:**
        This path operations get data user
        
    
    **Parameters:**
    
   

    **Returns:**
    - id (string) : identifier in the record
    - name (string): User name
    - age (string): age of the user
    - username (string): unique token per user
    """

    schema = userSchema(
        db.users.find_one({'_id': user_id})
    )

    return schema
