

def userSchema(item) -> dict:
    return {
        "id": str(item["_id"]),
        "name": item["name"],
        "age": item["age"],
        "username": item["username"],
    }



def userSchemaEntity(item) -> dict:
    return {
        "id": str(item["id"]),
        "name": item["name"],
        "age": item["age"],
        "username": item["username"],
    }

def userList(users) -> list:
    return [userSchema(user) for user in users]


