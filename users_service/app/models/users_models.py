from pydantic import BaseModel, Field

class UserBase(BaseModel):
    name: str = Field(
        ..., 
        min_length=1,
        max_length=50,
        example="Wilder Colmenares"
    )
    age: int = Field(
        ...,
        gt=0,
        le=115,
        example=25
    )
    username: str = Field(
        ..., 
        min_length=1,
        max_length=50,
        example="usuario1"
    )


class User(UserBase):
    password: str = Field(
        ..., 
        min_length=5
    )
    password_confirm: str = Field(
        ..., 
        min_length=5
    )
    disabled: bool = False


class UserOut(BaseModel):
    id: str
    user: UserBase



