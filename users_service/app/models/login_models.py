from pydantic import BaseModel, Field

class Login(BaseModel):
    username: str = Field(
        ...,
        example="usuario1"
    )
    password: str = Field(
        ..., 
        example="12121212"
    )