#FastAPI
from fastapi import APIRouter, Depends, Query, HTTPException
from typing import List
import pymongo

from users_service.app.config.common import get_current_user
from get_layaway_service.app.models.get_layaway_models import getLayaway
from users_service.app.config.db import db, db_comic
from get_layaway_service.app.schemas.get_layaway_schemas import layawaysSchemaList
from users_service.app.schemas.users_schema import userSchema




router = APIRouter(
    tags=["Extra's Layaway's"]
)
@router.get(
    "/getLayawayList",
    )
def get_layaway(
    user_id: str = Depends(get_current_user),
    search: str = Query(default=None),
    order_by:List[getLayaway] = Query(
        example=['title']
    ),
):  
    """
    Get Layaways

    **Description:**
        This path operations get layaways to a user as long as they are logged in
        
    
    **Parameters:**
    - search (string, required) name of comic
    - order_by (List, required): filter for order search
   

    **Returns:**
    - data (dict) :  comic list. according to the user and name searched
   
    """

    if search is None:
        raise HTTPException(status_code=400, detail="please enter a word")
    user_schema = userSchema(db.users.find_one({'_id':user_id}))

    comics = db_comic.layaway.find({"user.id":user_schema['id'], "comic.title":{"$regex":search, "$options":"i"}})

    print("layaways:", comics)

    layaways = layawaysSchemaList(comics)    

    return layaways

