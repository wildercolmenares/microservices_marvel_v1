
from users_service.app.schemas.users_schema import userSchema, userSchemaEntity
from search_service.app.schemas.search_comic_schema import comic_schema
from users_service.app.schemas.users_schema import userSchema, userSchemaEntity
from search_service.app.schemas.search_comic_schema import comic_schema


def layawaySchema(layaway) -> dict:
    return {
        "id": str(layaway["_id"]),
        "user": userSchemaEntity(layaway["user"]),
        "comic": comic_schema(layaway["comic"]),
    }


def layawaysSchemaList(layaways) -> list:
    return [layawaySchema(layaway) for layaway in layaways]