from fastapi import Depends, FastAPI
from search_service.app.routers import search_routers
from users_service.app.routers import users_routers
from add_layaway_service.app.routers import add_layaway_routers
from get_layaway_service.app.routers import get_layaway_routers

app = FastAPI()

app.include_router(users_routers.router)
app.include_router(search_routers.router)
app.include_router(add_layaway_routers.router)
app.include_router(get_layaway_routers.router)