Instrucciones para ejecutar el proyecto

1. Descargar la imagen de docker
    - sudo docker pull wildercolmenares/examentecnico:v1
    - Cualquier duda con la descarga, a continuación se muestra en el siguiente link https://hub.docker.com/r/wildercolmenares/examentecnico


2.  Levantar el servicio
    - sudo docker run -it --rm --name examentecnico -p 8000:8000 -v $(pwd):/marvel wildercolmenares/examentecnico:v1
    - Ver en: http://0.0.0.0:8000/docs

3. Para ejecutar las pruebas unitarias, se usó la librería de pytest.
    - Posicionarse en el directorio /microservices_marvel_v1
    - Ejecutar: pytest test_main.py
