#functions that return the values ​​of the API response
 
def character_schema(character) -> dict:
    return {
        "id": str(character["id"]),
        "name": str(character["name"]),
        "image": str(character["thumbnail"]["path"] + '.' + character["thumbnail"]["extension"]),
        "appearances": int(character["comics"]["available"]),
    }

def character_schema_list(characters) -> list:
    return [character_schema(character) for character in characters]