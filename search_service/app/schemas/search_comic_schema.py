#functions that return the values ​​of the API respons

def comic_schema(comic) -> dict:
    return {
        "id": str(comic["id"]),
        "title": str(comic["title"]),
        "image": str(comic["thumbnail"]["path"] + '.' + comic["thumbnail"]["extension"])\
            if 'thumbnail' in comic else comic['image'],
        "onSaleDate": str(comic["dates"][0]["date"]) if "dates" in comic else comic['onSaleDate'],
        # "characters": characters
    }

def comic_schema_list(comics) -> list:
    return [comic_schema(comic) for comic in comics]