#Python
import hashlib
from datetime import datetime
#Utils
from search_service.app.config import settings


def hash_api():
    ts = datetime.timestamp(datetime.now())
    keys = str(ts) + settings.PRIVATE_KEY + settings.PUBLIC_KEY
    hashed = hashlib.md5(bytes(keys, 'utf-8')) .hexdigest()
    
    return {"ts": ts, "hash": hashed}