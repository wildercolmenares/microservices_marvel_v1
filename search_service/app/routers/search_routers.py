#FasAPI
from fastapi import APIRouter, status, Query
#Python
import requests
#Pydantic
from typing import Optional
#Utils
from search_service.app.config.settings import API, PUBLIC_KEY
from search_service.app.config.hash import hash_api
#Service
from search_service.app.schemas import search_character_schema, search_comic_schema

#Routers fo search

router = APIRouter(
    tags=["Search Comics"]
)

@router.get(
    "/searchComics",
    summary = "Search by Characters or Comics",
    status_code = status.HTTP_200_OK,
    )
def get_all(
     filter: Optional[str] = Query(
        "characters",
        enum=("characters", "comics")
    ),
    search: Optional[str] = Query(
        None, 
        example = "namor"
    ),
     page: Optional[int] = Query(
        default=1
    ),
):
    """
    Search by comics or characters

    **Description:**
        This path operations search comics and characters from de API of Marvel
        
    
    **Parameters:**
    - search (string, optional): word search the title of a comic or a character. value can be nul.
    - page (string, optional) In case you don't want to search by words, you can browse and search according to page number
    - filter (string, optional) filter to search between Comics and Characters

    **Returns:**

    - Filter (character): the response is:
        - id: str -> character identifier
        - name: str -> character's name
        - image: str -> character image url
        - appearances -> number of appearances in comics

    - Filter (comics): the response is:
        - id: str -> comic identifier
        - title: str -> comic name
        - image: str -> comic image
        - onSaleDate -> release date
    """

    hashed = hash_api()
    data = {
        "ts":hashed['ts'],
        "apikey": PUBLIC_KEY,
        "hash":hashed['hash'],
        "limit":10,
        "offset": page - 1,
    }

    if filter == "characters":
        api_marvel = API + 'characters'
        if search:
            data["nameStartsWith"] = search
    if filter == "comics":
        api_marvel = API + 'comics'
        if search:
            data["titleStartsWith"] = search
            
    response = requests.get(api_marvel, params=data)
    
    if filter == "characters":
        response = search_character_schema.character_schema_list(response.json()["data"]["results"])
    
    if filter == "comics":
        response = search_comic_schema.comic_schema_list(response.json()["data"]["results"])

    return response