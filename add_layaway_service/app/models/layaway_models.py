#Pydantic
from pydantic import BaseModel, Field
#Python
from datetime import datetime
from typing import List
#Utils
from users_service.app.models.users_models import UserBase


class Character(BaseModel):
    id: str
    name: str
    image: str
    appearances: int


class Comic(BaseModel):
    id:str
    title:str
    image:str
    onSaleDate:datetime | None
    characters:List[Character] | None


class Layaway(BaseModel):
    user: UserBase
    comic: Comic

    class Config:
        orm_mode = True