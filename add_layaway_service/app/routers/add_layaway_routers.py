
#FastAPI
from fastapi import APIRouter, status, Depends, HTTPException
#Utils
from add_layaway_service.app.models.layaway_models import Layaway
from add_layaway_service.app.schemas.add_layaway_schemas import createLayawaySchema
from users_service.app.config.common import get_current_user
from users_service.app.config.db import db, db_comic
from search_service.app.config.settings import API, PUBLIC_KEY
from search_service.app.config.hash import hash_api
from users_service.app.schemas.users_schema import userSchema
#Python
import requests



router = APIRouter(
    tags=["Extra's Layaway's"]
)


@router.post(
    "/addToLayaway",
    # response_model = Layaway,
    status_code=status.HTTP_201_CREATED,
    )
def add_layaway(
    comic_id:str,
    user_id: str = Depends(get_current_user)
):
    """
    Add Layaway

    **Description:**
        This path operations add layaway to a user as long as they are logged in
        
    
    **Parameters:**
    - comic_id (string, required) ID of the comic you want to be separated
   

    **Returns:**
    - message (string) : message successful
    - comic (dict): dict of the data comic
   
    """

    api_marvel = API + 'comics/' + comic_id
    hashed = hash_api()
    data = {
        "ts":hashed['ts'],
        "hash":hashed['hash'],
        "apikey": PUBLIC_KEY,
    }

    response =  requests.get(api_marvel, params=data)
    if not response:
            raise HTTPException(status_code=404, detail="Please check it's a comic or ID is valid")

    user = userSchema(db.users.find_one({'_id':user_id}))
    print("user:", user)

    get_comic = response.json()["data"]["results"][0]

    create_layaway = createLayawaySchema({
        "user":user,
        "comic":get_comic,
    })

    insert_db = db_comic.layaway.insert_one(create_layaway)

    return {"message": "successful creation", "comic":create_layaway['comic']}