from fastapi.testclient import TestClient
from main import app
import json


client = TestClient(app)


####################### TESTING FOR SEARCH SERVICE #################################


def test_search_comics():
    response = client.get("/searchComics")
    assert response.status_code == 200



####################### TESTING FOR USERS SERVICE #################################

def test_create_register():
    data = json.dumps(
        {
            "name": "Usuario de prueba",
            "age": 26,
            "username": "testing1",
            "password": "12121212",
            "password_confirm": "12121212",
            "disabled": True
        }
    )
    response = client.post("/register", data=data)
    assert response.status_code == 201

def test_login():
    response = client.post("/login", data=json.dumps({
        "username":"testing1",
        "password":"12121212"        
    }))
    assert response.status_code == 200

def test_logout():
    response = client.get("/logout")
    assert response.status_code == 200


def test_get_user_me():
    response = client.post("/login", data=json.dumps({
        "username":"testing1",
        "password":"12121212"        
    }))
    response = client.get("/users/me")
    assert response.status_code == 200

def test_find_all_users():
    response = client.get("/users")
    assert response.status_code == 200



####################### TESTING FOR LAYAWAYs #################################


def test_get_layaway_list():
    response = client.post("/login", data=json.dumps({
        "username":"usuario4",
        "password":"12121212"        
    }))
    data = {
        "search":"namor",
        "order_by":["title"]
    }
    response = client.get("/getLayawayList", params=data)
    assert response.status_code == 200

def test_make_layaway():
    response = client.post("/login", data=json.dumps({
        "username":"testing1",
        "password":"12121212"        
    }))
    data = {
        "comic_id":"92216",
    }
    response = client.post("/addToLayaway", params=data)
    assert response.status_code == 201