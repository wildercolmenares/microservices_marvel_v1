# 
FROM python:3.10.6

# 
WORKDIR /marvel

# 
COPY ./requirements.txt /marvel/requirements.txt

# 
RUN pip install --no-cache-dir --upgrade -r /marvel/requirements.txt

# 
COPY . /marvel

# 
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000", "--reload"]
